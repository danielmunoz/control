* This Header file has been automaticaly
* generated from the C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\Control.hpp project files
* including:
*    ZP.s
*    TF.s
*    UTILS.s
*    FZaP.s
*    MJoin.s
*    CTRB.s
*    OBSV.s
*    SS2TF.s
*    ACKER.s
*    Routh.s
*    Bisec.s

ASSEMBLEM DC RomId 483 !RPL
xROMID 483

* entries of the file ZP.s
EXTERNAL xZP
EXTERNAL CreateZerosOnS

* entries of the file TF.s
EXTERNAL xTF
EXTERNAL CreatePolyOnS

* entries of the file UTILS.s
EXTERNAL x?toS
EXTERNAL xSto?
EXTERNAL xNUMDEN
EXTERNAL MaxAbs

* entries of the file FZaP.s
EXTERNAL xFZ&P
EXTERNAL ConvertToListIfNot

* entries of the file MJoin.s
EXTERNAL MJoin

* entries of the file CTRB.s
EXTERNAL xCTRB

* entries of the file OBSV.s
EXTERNAL xOBSV

* entries of the file SS2TF.s
EXTERNAL xSS2TF

* entries of the file ACKER.s
EXTERNAL xACKER
EXTERNAL GetMatrixPhi
EXTERNAL CreateLastOneRowMatrix

* entries of the file Routh.s
EXTERNAL xRouth
EXTERNAL NextRow
EXTERNAL EpsilonIfFirstZero
EXTERNAL AxiliaryPolyIfAllZeros
EXTERNAL AuxiliaryPoly
EXTERNAL CreateFirstTwoLines

* entries of the file Bisec.s
EXTERNAL xBisec
EXTERNAL GetK
EXTERNAL GetMagOfProducts
EXTERNAL AngleFromFPole
EXTERNAL BisecFromAnglePole
EXTERNAL NoKReport
EXTERNAL KReport
EXTERNAL RealPartOfPointForAngleFromPoleAngle
EXTERNAL FirstAngleFromAnglePole
EXTERNAL SecondAngleFromAnglePole
EXTERNAL NAngleFromAnglePole

