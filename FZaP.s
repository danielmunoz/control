RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\FZaP.s, part of the Control.hpp project, created by <> on 10/03/2015 )

INCLUDE Control.h

(
Find Zeros and Poles from Transfer Function
1: Transfer function
)
xNAME FZ&P
::
  CK1&Dispatch
  #00009 ( symb )
  ::  { LAM tFunc } BIND
    LAM tFunc
    xNUMDEN
    { LAM Num LAM Den } BIND
    xRCLF
    { LAM Flags } BIND
    ERRSET ::
      ZINT -105 xSF
      ZINT -103 xSF
      LAM Num
      ID S
      SETRAD ROMPTR xZEROS
      ConvertToListIfNot
      LAM Den
      ID S
      ROMPTR xZEROS
      ConvertToListIfNot
      LAM Flags xSTOF
    ; ERRTRAP ::
      LAM Flags xSTOF
    ;
  ;
;

NULLNAME ConvertToListIfNot
::
  FLASHPTR DUPCKLEN{}
  #1=
  IT ONE{}N
;

