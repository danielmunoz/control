RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\BAN.s, part of the Control.hpp project, created by <> on 12/03/2015 )

INCLUDE Control.h

xNAME CTRB
::
  CK2&Dispatch
  #00044 ( matrix matrix )
  :: { LAM A LAM B } BIND
    ERRSET
    ::
      LAM A FLASHPTR MDIMS LAM B FLASHPTR MDIMS #4 ROLL AND
      ITE
      :: { LAM RowsA LAM ColsA LAM RowsB LAM ColsB } BIND
        LAM RowsA LAM ColsA EQUAL LAM ColsA LAM RowsB EQUAL AND
        ITE
        ::
	  LAM B
          LAM ColsA
          BINT1
          DO
          ::
	    LAM A INDEX@ FLASHPTR #>Z FLASHPTR MAT^
	    LAM B
	    FLASHPTR MMMULT
	    MJoin
          ;
	  LOOP
        ;
        :: ( else )
          FLASHPTR ErrBadDim
        ;
      ;
      :: ( else )
        FLASHPTR ErrBadDim
      ;
    ;
    ERRTRAP
    ::
      FLASHPTR ErrBadDim
    ;
  ;
;

