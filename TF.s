RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\TF.s, part of the Control.hpp project, created by <> on 10/03/2015 )

INCLUDE Control.h

(
Create a transfer function from polynomy coefficients
)
xNAME TF
::
  CK2&Dispatch
  #00044 ( array array )
  :: { LAM Num LAM Den } BIND ( Save Num and Den )

    xRCLF
    :: { LAM Flags } BIND
      ERRSET ::
        LAM Num CreatePolyOnS
        LAM Den CreatePolyOnS ' x/ BINT3 SYMBN
        LAM Flags xSTOF
      ;
      ERRTRAP ::
        LAM Flags xSTOF
      ;
    ;
  ;
;

NULLNAME CreatePolyOnS
:: { LAM myArray } BIND
  LAM myArray
  MDIMSDROP
  BINT0
  DO
  ::
    LAM myArray ISTOP@ INDEX@ #- FLASHPTR PULLEL[S] SWAPDROP ( Get ith element )
    INDEX@ FLASHPTR #>Z ID S SWAP ' x^ ' x* BINT5 SYMBN
    INDEX@ #0<> IT :: ' x+ BINT3 SYMBN ;
  ;
  LOOP
  xEVAL
  ;

