RPL
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\BAT.s, part of the Control.hpp project, created by <> on 12/03/2015 )

INCLUDE Control.h

EXTERNAL xROW\->

xNAME OBSV
::
  CK2&Dispatch
  #00044 ( matrix matrix )
  :: { LAM A LAM C } BIND
    ERRSET
    ::
      LAM A FLASHPTR 2DMATRIX?
      ITE
      ::
        FLASHPTR MATTRAN
        LAM C FLASHPTR 2DMATRIX? NOT
        IT
        ::
	  ZINT 1 xROW\->
        ;
        FLASHPTR MATTRAN
        xCTRB
      ;
      :: ( else )
        FLASHPTR ErrBadDim
      ;
    ;
    ERRTRAP
    ::
      FLASHPTR ErrBadDim
    ;
  ;
;

