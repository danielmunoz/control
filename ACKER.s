RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\ACKER.s, part of the Control.hpp project, created by <> on 16/03/2015 )

INCLUDE Control.h

xNAME ACKER
::
  CK3&Dispatch
  #00444 ( matrix matrix array )
  :: { LAM A LAM B LAM J } BIND
    LAM J ROMPTR xPCOEF
    { LAM Alphas } BIND
    LAM A CreateLastOneRowMatrix
    LAM A LAM B xCTRB FLASHPTR MATINV
    FLASHPTR MAT*
    LAM A LAM Alphas GetMatrixPhi
    FLASHPTR MAT*
  ;
;

NULLNAME GetMatrixPhi
:: { LAM A LAM Alphas } BIND
  LAM Alphas DUP
  MDIMSDROP
  FLASHPTR PULLEL[S] SWAPDROP
  LAM A MDIMSDROP DROP FLASHPTR MATIDN
  FLASHPTR SCL*MAT

  LAM Alphas
  MDIMSDROP
  BINT1
  DO
  ::
    LAM Alphas ISTOP@ INDEX@ #- FLASHPTR PULLEL[S] SWAPDROP ( Get ith element )
    LAM A INDEX@ FLASHPTR #>Z FLASHPTR MAT^
    FLASHPTR SCL*MAT
    FLASHPTR MAT+
  ;
  LOOP
;

NULLNAME CreateLastOneRowMatrix
::
  MDIMSDROP
  Z0_ SWAP
  BINT1 SWAP
  TWO{}N
  FLASHPTR OBJDIMS2MAT
  Z1_
  UNROT
  FLASHPTR BANGARRY
;


