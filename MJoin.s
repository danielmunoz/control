RPL

INCLUDE Control.h

( Joins two matrix: column augment )
NULLNAME MJoin
::
  CK2&Dispatch
  #00044 ( matrix matrix )
  :: { LAM M1 LAM M2 } BIND
    LAM M1 FLASHPTR MDIMS
    ITE :: { LAM M1Rows LAM M1Cols } BIND
      LAM M2 FLASHPTR MDIMS
      ITE :: { LAM M2Rows LAM M2Cols } BIND
        LAM M1 LAM M2
	LAM M1Rows LAM M2Rows #=
	IT
	::
	  LAM M1Cols #1+ FLASHPTR #>Z
	  ERRSET
	  ::
	    ROMPTR xCOL+
	  ;
	  ERRTRAP
	  ::
	    3DROP
	  ;
	;
      ; :: DROP LAM M1 LAM M2 ; ( else )
    ; :: DROP LAM M1 LAM M2 ; ( else )
  ;
;

