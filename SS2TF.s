RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\SS2TF.s, part of the Control.hpp project, created by <> on 13/03/2015 )

INCLUDE Control.h

xNAME SS2TF
::
  CK4&Dispatch
  #04444  ( matrix matrix matrix matrix )
  :: { LAM mA LAM mB LAM mC LAM mD } BIND
    LAM mA FLASHPTR MDIMS UNROT 2DUP EQUAL #4 ROLL AND SWAPDROP
    IT ( if Anxn )
    :: { LAM nA } BIND
      ID S LAM nA FLASHPTR MATIDN FLASHPTR SCL*MAT
      LAM mA FLASHPTR MAT-
      FLASHPTR MATINV
      LAM mC SWAP FLASHPTR MAT*
      LAM mB FLASHPTR MAT*
      LAM mD FLASHPTR MAT+
    ;
  ;
;

