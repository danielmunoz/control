RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\UTILS.s, part of the Control.hpp project, created by <> on 10/03/2015 )

INCLUDE Control.h

( Converts variable var to S
2: Object
1: var )
xNAME ?toS
::
  CK2&Dispatch
  #00006 ( ob id )
  ::  { LAM var } BIND
    LAM var ID S x= FLASHPTR SYMBEXEC
  ;
;

( Converts S to variable var
2: Object
1: var )
xNAME Sto?
::
  CK2&Dispatch
  #00006 ( ob id )
  ::  { LAM var } BIND
    ID S LAM var x= FLASHPTR SYMBEXEC
  ;
;

xNAME NUMDEN
::
  xOBJ>
  ' x/ EQUAL
  SWAP %2 EQUAL
  AND NOT
  IT FLASHPTR TRANSCERROR
;

( Not in use, just a scratch )
( finds maximum absolute value of expr for var = jw )
( 2: expr )
( 1: var )
NULLNAME MaxAbs
:: { LAM tFunc LAM var } BIND
  LAM tFunc LAM var ID i ID w x* x=
  FLASHPTR SYMBEXEC
  DUP
  FLASHPTR DENOLCMext
  xCONJ
  DUPUNROT
;
