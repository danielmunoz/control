RPL
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\TF.s, part of the Control.hpp project, created by <> on 06/03/2015 )

INCLUDE Control.h

(
Creates transfer function from array of zeros and array of poles.
The array of zeros can be a number instead of an array if there are
no zeros.
2: [Zeros] or a number
1: [Poles]
)
xNAME ZP
::
  CK2&Dispatch
  #00044 ( array array )
  :: { LAM Num LAM Den } BIND ( Save Num and Den )
    xRCLF
    :: { LAM Flags } BIND
      ERRSET ::
        LAM Num CreateZerosOnS
        LAM Den CreateZerosOnS ' x/ BINT3 SYMBN
      ;
      ERRTRAP ::
        LAM Flags xSTOF
      ;
    ;
  ;
  
  #00014 ( int array )
  :: { LAM Num LAM Den } BIND ( Save Num and Den )
    xRCLF
    :: { LAM Flags } BIND
      ERRSET ::
        LAM Num BINT1 SYMBN
        LAM Den CreateZerosOnS ' x/ BINT3 SYMBN
      ;
      ERRTRAP ::
        LAM Flags xSTOF
      ;
    ;
  ;
  
  #00FF4 ( int array )
  :: { LAM Num LAM Den } BIND ( Save Num and Den )
    xRCLF
    :: { LAM Flags } BIND
      ERRSET ::
        LAM Num BINT1 SYMBN
        LAM Den CreateZerosOnS ' x/ BINT3 SYMBN
      ;
      ERRTRAP ::
        LAM Flags xSTOF
      ;
    ;
  ;
;

NULLNAME CreateZerosOnS
:: { LAM myArray } BIND
  LAM myArray
  MDIMSDROP
  BINT0
  DO
  ::
    LAM myArray INDEX@ #1+ FLASHPTR PULLEL[S] SWAPDROP ( Get ith element )
    ID S SWAP ' x- BINT3 SYMBN
    INDEX@ BINT0 EQUAL NOT IT :: ' x* BINT3 SYMBN ;
  ;
  LOOP
  xEVAL
  ;

( stop start DO <loop clause> LOOP  )
