RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\Bisec.s, part of the Control.hpp project, created by <> on 01/04/2015 )

INCLUDE Control.h

xNAME Bisec
::
  CK2&Dispatch
  #00092 ( symb cmplx )
  :: { LAM F LAM DesiredPole } BIND
    LAM F LAM DesiredPole AngleFromFPole
    %180 SWAP %-
    LAM DesiredPole BisecFromAnglePole
    LAM F LAM DesiredPole GetK
    KReport
  ;
  
  #00012 ( real cmplx )
  ::
    BisecFromAnglePole
    NoKReport
  ;
  
  #00FF2 ( zint cmplx )
  ::
    SWAP FLASHPTR Z>R SWAP
    BisecFromAnglePole
    NoKReport
  ;
;

NULLNAME GetK
:: { LAM ZeroTagged LAM PoleTagged LAM F LAM DesiredPole } BIND
  LAM ZeroTagged xEVAL C>Re%
  LAM PoleTagged xEVAL C>Re%
  { LAM Zero LAM Pole } BIND
  LAM ZeroTagged LAM PoleTagged
  LAM F
  xFZ&P
  SWAP
  LAM Zero FLASHPTR AppendList
  LAM DesiredPole GetMagOfProducts
  SWAP
  LAM Pole FLASHPTR AppendList
  LAM DesiredPole GetMagOfProducts
  %/ %1 SWAP %/
  "K" >TAG
;

NULLNAME GetMagOfProducts
:: { LAM DesiredPole } BIND
  FLASHPTR CKINNERCOMP
  %1 SWAP
  BINT0
  DO
  ::
    SWAP LAM DesiredPole SWAP x- C%ABS %*
  ;
  LOOP
;

NULLNAME AngleFromFPole
::
  ID S SWAP x= FLASHPTR SYMBEXEC xEVAL C%ARG
;

NULLNAME BisecFromAnglePole
:: { LAM Angle LAM DesiredPole } BIND
  LAM DesiredPole
  LAM Angle LAM DesiredPole FirstAngleFromAnglePole
  DUP { LAM AngleZero } BIND
  RealPartOfPointForAngleFromPoleAngle
  Re>C% { LAM Zero } BIND
  LAM DesiredPole
  LAM Angle LAM DesiredPole SecondAngleFromAnglePole
  DUP { LAM AnglePole } BIND
  RealPartOfPointForAngleFromPoleAngle
  Re>C% { LAM Pole } BIND
  LAM DesiredPole "Desired pole" >TAG
  LAM Angle "Needed angle" >TAG
  LAM AngleZero "Zero Angle" >TAG
  LAM AnglePole "Pole Angle" >TAG
  LAM Zero "Zero" >TAG
  LAM Pole "Pole" >TAG
;

NULLNAME NoKReport
::
  #7 #1-{}N
;

NULLNAME KReport
::
  #8 #1-{}N
;

NULLNAME RealPartOfPointForAngleFromPoleAngle
::
  SWAP DUP C>Re% SWAP C>Im% ROT %TAN %/ %-
;

NULLNAME FirstAngleFromAnglePole
::
  NAngleFromAnglePole %+
;

NULLNAME SecondAngleFromAnglePole
::
  NAngleFromAnglePole %-
;

NULLNAME NAngleFromAnglePole
::
  C%ARG %2 %/ SWAP %2 %/
;
