# README #
I created an HP 50g lib with some tools similar to Matlab tools for Control Systems Engineering.

## Install ##
Copy [Control.hp](https://bitbucket.org/danielmunoz/control/downloads/Control.hp) file (generated bin) to an SD Card and insert the SD Card into the calculator. With the filer (Left Shift + FILES) go to the SD Card and Copy the file into Port 0, 1 or 2. It will be Lib 1155. Press ON + F3 to restart the calculator and attach the lib:

```
#!Stack

1: 1155
ATTACH
```


Or you can create an automatic attach script saving STARTUP variable in the HOME dir:

```
#!Stack

2: << 1155 ATTACH >>
1: 'STARTUP'
->STO
```


## Usage ##
This command will bring the menu of the lib (once it is installed and attached):

```
#!Stack

1: 1155
MENU
```


Or you can just write the names of the commands as you would do with any other built in command.

## Commands ##
### ZP ###
Similar to Matlab's zpk, but without k.

```
#!Stack

2: [1 2 3]
1: [4 5 6]
ZP
```

This will create a transfer function on 'S' with zeros: 1, 2 and 3, and with poles: 4, 5, 6.

You can also put a number as the first parameter:

```
#!Stack

2: 4
1: [4 5 6]
ZP
```

So it will be 4/P(S)

### TF ###
Similar to Matlab's tf.
It generates a transfer function with the coefficients of the polynomials of the numerator and denominator:

```
#!Stack

2: [1 2 3]
1: [4 5 6] 
TF
```

This will generate:
(S^2 + 2 * S + 3) / (4*S^2 + 5 * S + 6)

### FZ&P ###
Find zeros and poles of a transfer function of the form Num(S)/Den(S).

```
#!Stack

1: Num(S)/Den(S)
FZ&P
```

### CTRB ###
Similar to Matlab's CTRB.

```
#!Stack

2: Matrix A
1: Matrix B
CTRB
```

The result will be the controlability matrix.

### OBSV ###
Similar to Matlab's OBSV.

```
#!Stack

2: Matrix A
1: Matrix B
OBSV
```

The result will be the observability matrix.

### SS2TF ###
Similar to Matlab's ss2tf (Convert state-space representation to transfer function). But it returns the entire matrix of transfer functions (for multiple inputs and outputs).

```
#!Stack

4: Matrix A
3: Matrix B
2: Matrix C
1: Matrix D
SS2TF
```

The result will be the matrix of transfer functions for each output/input pair.

For example for a 2 inputs and 2 outputs system, it will be:
[ Y1/U1, Y2/U1 ]
[ Y1/U2, Y2/U2 ]

### ?toS ###
Converts all 'var' variables in an expression to 'S'

```
#!Stack

2: Object
1: var
?toS
```

The result will be the same object but with all their 'var' variables converted to 'S'

### Sto? ###
Converts all 'S' variables in an expression to 'var' variable

```
#!Stack

2: Object
1: var
Sto?
```

The result will be the same object but with all their 'S' variables converted to 'var'

### ACKER ###
Pole placement gain selection using Ackermann's formula. Similar to matlab's acker command. Inputs are matrix A, matrix B and Vector J, which is the vector of desired closed loop poles.

```
#!Stack

3: A
2: B
1: J
ACKER
```

The result will be the matrix K is the feedback gain matrix.

### NUMDEN ###
Splits object into numerator and denominator. Similar to matlab's numden

```
#!Stack

1: 'Num/Den'
NUMDEN
```

The result will be:
2: Num
1: Den

### Routh ###
Generates a matrix with Routh-Hurwitz elements from a vector of polynomial coefficients.

```
#!Stack

1: [a1 a2 a3...]
Routh
```

The result will be:

1: [[coefficients]]

### Bisec ###
Helps to apply Bisector method for lead compensation.

First usage:
```
#!Stack

2: F(S)
1: DesiredPole(must be a complex number)
Bisec
```

The result will be a list reporting:
Desired Pole
Needed angle (lead)
Zero Angle (angle of the zero)
Pole Angle (angle of the pole)
Zero (location of the zero)
Pole (location of the pole)
K (Needed K to comply with magnitude condition for the desired pole)

Second usage:
```
#!Stack

2: Desired lead angle (real or integer)
1: DesiredPole(must be a complex number)
Bisec
```

The result will be a list reporting the same as above but without K. The angle can be in degrees or radians, according to the "Angle measure" option configured in the calculator. Grads should work too.

# Who do I talk to? #
Daniel Muñoz

daniel.munoz.trejo@gmail.com