RPL 
( C:\Users\DanielAngel\Documents\Debug4x\Projects\Control\Routh.s, part of the Control.hpp project, created by <> on 22/03/2015 )

INCLUDE Control.h

xNAME Routh
::
  CK1&Dispatch
  #00004 ( array )
  :: { LAM Array } BIND
    LAM Array
    CreateFirstTwoLines
    LAM Array MDIMSDROP #1-
    #0
    AxiliaryPolyIfAllZeros
    EpsilonIfFirstZero
    LAM Array
    MDIMSDROP #2-
    BINT0
    DO
    ::
      NextRow
      LAM Array MDIMSDROP #1-
      INDEX@ #1+
      AxiliaryPolyIfAllZeros
      EpsilonIfFirstZero
    ;
    LOOP
    BINT1 ONE{}N
    FLASHPTR OBJDIMS2MAT
    LAM Array MDIMSDROP #1-
    BINT0
    DO
    ::
      SWAP BINT1
      FLASHPTR INSERTROW[]
    ;
    LOOP
  ;
;

NULLNAME NextRow
::
  { LAM P1 LAM P2 } BIND
  LAM P1 LAM P2
  LAM P1 MDIMSDROP ONE{}N Z0_
  FLASHPTR MAKEARRY
  LAM P1 #1 FLASHPTR PULLEL[S] SWAPDROP
  LAM P2 #1 FLASHPTR PULLEL[S] SWAPDROP
  { LAM A LAM B } BIND
  LAM P1 MDIMSDROP
  BINT1
  DO
  ::
    LAM P1 INDEX@ #1+ FLASHPTR PULLEL[S] SWAPDROP
    LAM P2 INDEX@ #1+ FLASHPTR PULLEL[S] SWAPDROP
    { LAM C LAM D } BIND
    LAM B LAM C x* LAM A LAM D x* x- LAM B x/ xEVAL
    INDEX@
    ROT
    FLASHPTR PUT[]
  ;
  LOOP
;

NULLNAME EpsilonIfFirstZero
::
  #1
  FLASHPTR PULLEL[S]
  DUP Z0_ EQUAL
  SWAP %0 EQUAL
  OR
  IT
  ::
    ID \93 #1 ROT FLASHPTR PUT[]
  ;
;

NULLNAME AxiliaryPolyIfAllZeros
:: { LAM PrevRow LAM Row LAM MaxPower LAM LessPower } BIND
  LAM Row
  MDIMSDROP
  FALSE SWAP
  BINT0
  DO
  ::
    LAM Row
    INDEX@ #1+
    FLASHPTR PULLEL[S] SWAPDROP
    DUP Z0_ EQUAL
    SWAP %0 EQUAL
    OR
    NOT ( diff than zero )
    OR ( if any is diff than zero )
  ;
  LOOP
  NOT ITE
  :: ( if all zero )
    LAM PrevRow
    LAM MaxPower
    LAM LessPower
    AuxiliaryPoly
  ;
  ::
    LAM Row
  ;
  LAM PrevRow SWAP
;

NULLNAME AuxiliaryPoly
:: { LAM Poly LAM MaxPower LAM LessPower } BIND
  LAM Poly MDIMSDROP
  ONE{}N Z0_
  FLASHPTR MAKEARRY
  LAM MaxPower LAM LessPower #- #2 #/ #+
  BINT0
  DO
  ::
    LAM Poly
    INDEX@ #1+
    FLASHPTR PULLEL[S] SWAPDROP
    LAM MaxPower LAM LessPower #- INDEX@ #2* #-
    FLASHPTR #>Z
    x* xEVAL
    INDEX@ #1+
    ROT
    FLASHPTR PUT[]
  ;
  LOOP
;

NULLNAME CreateFirstTwoLines
:: { LAM Array } BIND
  LAM Array MDIMSDROP #2 #/ #+
  ONE{}N Z0_
  FLASHPTR MAKEARRY DUP
  LAM Array MDIMSDROP
  BINT0
  DO
  ::
    LAM Array
    INDEX@ #1+
    FLASHPTR PULLEL[S] SWAPDROP
    INDEX@ #2 #/ DROP #0=
    ITE
    :: ( Even )
      ROTSWAP
      INDEX@ #2 #/ SWAPDROP #1+ ROT
      FLASHPTR PUT[]
      SWAP
    ;
    :: ( Odd )
      INDEX@ #2 #/ SWAPDROP #1+ ROT
      FLASHPTR PUT[]
    ;
  ;
  LOOP
  LAM Array MDIMSDROP #2 #/ DROP #0<> IT :: Z0_ FLASHPTR AppendList ;
;

